# Scala Chess

 Chess game with a basic minimax search computer opponent. Uses https://gitlab.com/hansed/scalaengine for rendering.

![](https://gitlab.com/hansed/scalachess/uploads/d9daadd284042a5774062c9e76bc532a/Kuvakaappaus_2019-12-07_22-37-59.png)