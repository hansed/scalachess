package org.hansed.chess.board.computer

import org.hansed.chess.board.Piece
import org.hansed.chess.{PieceColor, PieceType}

/**
  * The piece stored on [[org.hansed.chess.board.computer.ComputerBoard]]
  *
  * @param pieceType the type of the piece
  * @param color     the color of the piece
  * @author Hannes Sederholm
  */
class ComputerPiece(val pieceType: PieceType, val color: PieceColor) extends Piece