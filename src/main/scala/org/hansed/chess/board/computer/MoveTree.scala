package org.hansed.chess.board.computer

import org.hansed.chess._
import org.hansed.chess.board.{Board, Piece}
import org.hansed.chess.evaluation.Evaluator
import org.hansed.chess.moves.ChessMove

import scala.collection.mutable

/**
  * A tree structure for keeping track of the move available and their scoring
  *
  * @param turn      the turn color of this layer on the move tree
  * @param evaluator the evaluator used to score branches
  * @author Hannes Sederholm
  */
case class MoveTree(var turn: PieceColor)(implicit evaluator: Evaluator) {

  val children: mutable.Map[ChessMove, MoveTree] = mutable.HashMap()


  var bestMove: Option[ChessMove] = None

  def update(board: Board[_ <: Piece], depth: Int): Unit = {
    val tempBoard = new ComputerBoard()
    tempBoard.copyState(board)
    score(tempBoard, depth)
  }

  def select(board: Board[_ <: Piece], move: ChessMove): MoveTree = {
    children.getOrElse(move, MoveTree(board.turn))
  }

  /**
    * Find the score of the tree
    *
    * updates bestMove with the currently bestMove
    *
    * @return least bad score from all the branches
    */
  def score(board: ComputerBoard, depth: Int, a: Float = Float.MinValue, b: Float = Float.MaxValue): Float = {
    if (depth == 0) {
      evaluator.evaluate(board)
    } else {
      var alpha = a
      var beta = b
      var bestScore: Float = if (turn == WHITE) Float.MinValue else Float.MaxValue

      if (children.isEmpty) {
        init(board)
      }
      if (turn == WHITE) {
        for ((move, child) <- children) {
          board.performMove(move)
          val score = child.score(board, depth - 1, alpha, beta)
          if (score > alpha) {
            alpha = score
          }
          if (score > bestScore) {
            bestScore = score
            bestMove = Some(move)
          }
          board.undo()
          if (alpha >= beta) {
            return bestScore
          }
        }
      } else {
        for ((move, child) <- children) {
          board.performMove(move)
          val score = child.score(board, depth - 1, alpha, beta)
          if (score < bestScore) {
            bestScore = score
            bestMove = Some(move)
          }
          if (score < beta) {
            beta = score
          }
          board.undo()
          if (alpha >= beta) {
            return bestScore
          }
        }

      }
      bestScore
    }
  }

  /**
    * Initialize the tree with the given board
    *
    * @param board the board whose moves are to be considered
    */
  def init(board: Board[_ <: Piece]): Unit = {
    val tempBoard = new ComputerBoard()
    tempBoard.copyState(board)
    for {
      x <- 0 until 8
      y <- 0 until 8
    } {
      if (board(x, y).exists(_.color == turn)) {
        board.availableTiles(Point(x, y)).foreach(tile => {
          val turnBefore = tempBoard.turnIndex
          tempBoard.move(x, y, tile.x, tile.y).foreach(move => {
            tempBoard.performMove(move)
            if (tempBoard.turnIndex != turnBefore) {
              children.put(move, MoveTree(if (turn == WHITE) BLACK else WHITE))
              tempBoard.undo()
            }
          })
        })
      }
    }
  }

}
