package org.hansed.chess.board.computer

import org.hansed.chess._
import org.hansed.chess.board.{Board, Piece}
import org.hansed.chess.moves._

/**
  * Represents the board the computer player uses when it is evaluating different moves
  *
  * @author Hannes Sederholm
  */
class ComputerBoard extends Board[ComputerPiece] {

  lazy val pieces: Array[Array[Option[ComputerPiece]]] = Array.fill(8, 8)(None)

  /**
    * Compy the state of given board to this board
    *
    * @param board the other board
    */
  def copyState(board: Board[_ <: Piece]): Unit = {

    var x = 0
    while (x < 8) {
      var y = 0
      while (y < 8) {
        pieces(x)(y) = board(x, y).map(createFromPiece)
        y += 1
      }
      x += 1
    }

    this.turnIndex = board.turnIndex
    this.turn = board.turn
  }

  /**
    * Make a move from given point to another point
    *
    * @param fromX start x coordinate
    * @param fromY start y coordinate
    * @param toX   end x coordinate
    * @param toY   end y coordinate
    * @return the move recognized from the given coordinate transform
    */
  def move(fromX: Int, fromY: Int, toX: Int, toY: Int): Option[ChessMove] = {
    apply(fromX, fromY).flatMap(piece => {
      piece.pieceType match {
        case PAWN if fromX != toX && apply(toX, toY).isEmpty =>
          Some(PassingMove(Point(fromX, fromY), Point(toX, toY)))
        case PAWN if toY == 0 || toY == 7 =>
          Some(PromotionMove(Point(fromX, fromY), Point(toX, toY), QUEEN, piece.color))
        case KING =>
          val xDiff = toX - fromX
          if (xDiff > 1) {
            Some(CastlingMove(turn, queenSide = false))
          } else if (xDiff < -1) {
            Some(CastlingMove(turn, queenSide = true))
          } else {
            Some(NormalMove(Point(fromX, fromY), Point(toX, toY)))
          }
        case _ => Some(NormalMove(Point(fromX, fromY), Point(toX, toY)))
      }
    })
  }

  /**
    * Create an instance of a given piece type
    *
    * @param pieceType the type of the piece
    * @param color     the color of the piece
    * @return an instance of piece for this board
    */
  override def createPiece(pieceType: PieceType, color: PieceColor): ComputerPiece = {
    new ComputerPiece(pieceType, color)
  }

  /**
    * Move a piece from one position to other
    *
    * Removes any piece at the target position
    *
    * @param from starting point
    * @param to   ending point
    */
  override def movePiece(from: Point, to: Point): Unit = {
    val was = pieces(from.x)(from.y)
    pieces(from.x)(from.y) = None
    was.foreach(was => {
      was.previousTile = Some(from)
      was.lastMoved = turnIndex

    })
    pieces(to.x)(to.y) = was
  }

  /**
    * Remove the piece at given point
    *
    * @param at the tile to clear
    */
  override def removePiece(at: Point): Unit = {
    pieces(at.x)(at.y) = None
  }

  /**
    * Add a piece to the board
    *
    * @param piece the piece
    * @param to    the point
    */
  override def addPiece(piece: Piece, to: Point): Unit = {
    piece match {
      case piece: ComputerPiece =>
        pieces(to.x)(to.y) = Some(piece)
      case _ =>
        pieces(to.x)(to.y) = Some(createFromPiece(piece))
    }
  }

  /**
    * Get piece at given coordinates
    *
    * @param x coordinate
    * @param y coordinate
    * @return the piece at point or None if nothing is located at coordiantes
    */
  override def apply(x: Int, y: Int): Option[ComputerPiece] = {
    pieces(x)(y)
  }
}
