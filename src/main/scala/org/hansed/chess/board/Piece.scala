package org.hansed.chess.board

import org.hansed.chess.{PieceColor, PieceType, Point}

/**
  * Trait defining a piece on the [[org.hansed.chess.board.Board]]
  *
  * @author Hannes Sederholm
  */
trait Piece {

  /**
    * Color of the piece
    */
  val color: PieceColor

  /**
    * Type of the piece
    */
  val pieceType: PieceType

  /**
    * The tile this piece was previously on
    */
  var previousTile: Option[Point] = None

  /**
    * The turn that this piece was last moved on
    */
  var lastMoved: Int = -1

  /**
    * Create a copy of this piece
    *
    * @return the copy
    */
  def copy: Piece = {
    val c = color
    val pt = pieceType
    new Piece {
      /**
        * Color of the piece
        */
      override val color: PieceColor = c
      /**
        * Type of the piece
        */
      override val pieceType: PieceType = pt

      this.previousTile = previousTile
      this.lastMoved = lastMoved
    }
  }

}
