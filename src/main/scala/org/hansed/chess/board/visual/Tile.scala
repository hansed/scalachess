package org.hansed.chess.board.visual

import org.hansed.chess.{blackMaterial, whiteMaterial}
import org.hansed.engine.core.eventSystem.Clickable
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.saving.ModelDefinition
import org.joml.Vector3f

/**
  * Represents a single tile on the board
  *
  * @param white the color of the tile
  * @author Hannes Sederholm
  */
class Tile(x: Int, y: Int, white: Boolean) extends Model(
  ModelDefinition("models/slab.dae", "", animated = false, if (white) whiteMaterial else blackMaterial)
) with Clickable {

  position = new Vector3f(x, 0, -y)

  private var _piece: Option[ModelPiece] = None

  def piece: Option[ModelPiece] = _piece

  def piece_=(piece: Option[ModelPiece]): Unit = {
    _piece.foreach(removeChild(_))
    _piece = piece
    piece.foreach(addChild(_))
  }

  def coordinates: (Int, Int) = (position.x.toInt, -position.z.toInt)

}
