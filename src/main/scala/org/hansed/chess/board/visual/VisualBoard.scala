package org.hansed.chess.board.visual

import org.hansed.chess._
import org.hansed.chess.board.Board.{MadeMove, MakeMove}
import org.hansed.chess.board.{Board, Piece}
import org.hansed.chess.moves._
import org.hansed.chess.ui.PromotionMenu
import org.hansed.chess.ui.PromotionMenu.{SelectPromotion, SelectedPromotion}
import org.hansed.engine.Engine
import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.ObjectClick
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.rendering.camera.EntityFollowingCamera
import org.hansed.engine.util.interpolation.Interpolator

/**
  * A 3d chess board visible to the user
  *
  * @author Hannes Sederholm
  */
class VisualBoard(computer: Option[ComputerPlayer]) extends GameObject with Board[ModelPiece] with EventListener {

  {
    addChild(new PromotionMenu)

    reset()

    computer.foreach(_.board = this)
    Events.signal(MakeMove(WHITE))
  }

  /**
    * The graphical tiles of the chess board
    */
  lazy val tiles: Array[Array[Tile]] = (0 until 8).map(x => {
    (0 until 8).map(y => {
      val tile = new Tile(x, y, (y + x) % 2 == 1)
      addChild(tile)
      tile
    }).toArray
  }).toArray

  /**
    * The current piece selected by clicking it
    */
  var selectedPiece: Option[ModelPiece] = None


  /**
    * Override changing turn to rotate camera to be facing from the turn direction
    *
    * @param color the new turn holder
    */
  override def turn_=(color: PieceColor): Unit = {
    super.turn_=(color)
    if (computer.isEmpty) {
      Engine.camera match {
        case camera: EntityFollowingCamera =>
          color match {
            case WHITE =>
              Interpolator.animate(
                camera.verticalSpin, 0.0f, (v: Float) => {
                  camera.verticalSpin = v
                }, 1000, tickLength = 10)
            case BLACK =>
              Interpolator.animate(
                camera.verticalSpin, 3.14f, (v: Float) => {
                  camera.verticalSpin = v
                }, 1000, tickLength = 10)
            case _ =>
          }
        case _ =>
      }
    }
  }

  /**
    * Create an instance of a given piece type
    *
    * @param pieceType the type of the piece
    * @param color     the color of the piece
    * @return an instance of piece for this board
    */
  override def createPiece(pieceType: PieceType, color: PieceColor): ModelPiece = {
    new ModelPiece(
      pieceType match {
        case KING => "models/king.dae"
        case QUEEN => "models/queen.dae"
        case ROOK => "models/rook.dae"
        case KNIGHT => "models/knight.dae"
        case BISHOP => "models/bishop.dae"
        case PAWN => "models/pawn.dae"
      },
      pieceType,
      color
    )
  }

  /**
    * Move a piece from one position to other
    *
    * Removes any piece at the target position
    *
    * @param from starting point
    * @param to   ending point
    */
  override def movePiece(from: Point, to: Point): Unit = {
    tiles(from.x)(from.y).piece.foreach(p => {
      p.previousTile = Some(from)
      p.lastMoved = turnIndex
      tiles(from.x)(from.y).piece = None
      tiles(to.x)(to.y).piece = Some(p)
    })
  }

  /**
    * Remove the piece at given point
    *
    * @param at the tile to clear
    */
  override def removePiece(at: Point): Unit = {
    tiles(at.x)(at.y).piece = None
  }

  /**
    * Add a piece to the board
    *
    * @param piece the piece
    * @param to    the point
    */
  override def addPiece(piece: Piece, to: Point): Unit = {
    piece match {
      case piece: ModelPiece =>
        tiles(to.x)(to.y).piece = Some(piece)
      case _ =>
        tiles(to.x)(to.y).piece = Some(createFromPiece(piece))
    }
  }

  /**
    * Get piece at given coordinates
    *
    * @param x coordinate
    * @param y coordinate
    * @return the piece at point or None if nothing is located at coordiantes
    */
  override def apply(x: Int, y: Int): Option[ModelPiece] = {
    tiles(x)(y).piece
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case ObjectClick(o, _) =>
        if (promotionState.isEmpty && !computer.exists(_.color == turn)) {
          var piece: Option[ModelPiece] = None

          // Extract the piece clicked
          o match {
            case p: ModelPiece => piece = Some(p)

            case tile: Tile =>
              val (x, y) = tile.coordinates
              if (tiles(x)(y).piece.isEmpty) {
                selectedPiece.foreach(p => {
                  move(p, x, y)
                })
              } else piece = tiles(x)(y).piece
            case _ =>
              deselectPiece()
          }

          // Process the result of clicking the piece
          piece.foreach(piece => {
            if (piece.color == turn) {
              selectPiece(piece)
            } else {
              selectedPiece.foreach(p => {
                piece.tile.foreach({
                  case (x: Int, y: Int) =>
                    move(p, x, y)
                })

              })
            }
          })
        }
      case MadeMove(_, board) =>
        if (board == this)
          Events.signal(MakeMove(turn))
      case SelectedPromotion(promotion) => finishPromotion(promotion)
      case _ =>
    }
  }

  def move(piece: ModelPiece, x: Int, y: Int): Unit = {

    var move: Option[ChessMove] = None

    piece.tile.foreach({
      case (tileX, tileY) =>
        if (availableTiles(Point(tileX, tileY)).contains(Point(x, y))) {
          piece.pieceType match {
            case PAWN if tileX != x && apply(x, y).isEmpty =>
              move = Some(PassingMove(Point(tileX, tileY), Point(x, y)))
            case PAWN if y == 0 || y == 7 =>
              beginPromotion(Point(tileX, tileY), Point(x, y))
            case KING =>
              val xDiff = piece.tile.map(x - _._1)
              xDiff.foreach(xDiff => {
                if (xDiff > 1) {
                  move = Some(CastlingMove(turn, queenSide = false))
                } else if (xDiff < -1) {
                  move = Some(CastlingMove(turn, queenSide = true))
                } else {
                  move = Some(NormalMove(Point(tileX, tileY), Point(x, y)))
                }
              })
            case _ =>
              move = Some(NormalMove(Point(tileX, tileY), Point(x, y)))
          }
        }
    })

    move.foreach(move => {
      performMove(move)
    })
    deselectPiece()
  }

  var promotionState: Option[(Point, Point, PieceColor)] = None

  /**
    * Begin the promotion of a piece
    *
    */
  def beginPromotion(prev: Point, next: Point): Unit = {
    promotionState = Some(prev, next, turn)
    Events.signal(SelectPromotion())
  }

  /**
    * Finish the promotion when a type has been chosen
    *
    * @param promotion the type of the promoted piece
    */
  def finishPromotion(promotion: PieceType): Unit = {
    promotionState.foreach({
      case (prev, next, color) =>
        performMove(PromotionMove(prev, next, promotion, color))
    })
    promotionState = None
  }

  /**
    * Select a piece
    *
    * Highlight tiles the piece can move into
    *
    * @param piece the piece
    */
  def selectPiece(piece: ModelPiece): Unit = {
    // Clear selected piece before selecting another one
    deselectPiece()
    selectedPiece = Some(piece)
    piece.setHighlight(true)
    piece.tile.foreach({
      case (x, y) =>
        availableTiles(Point(x, y)).foreach(p => {
          tiles(p.x)(p.y).setHighlight(true)
        })
    })
  }


  /**
    * Clear the selection of a piece
    */
  def deselectPiece(): Unit = {
    selectedPiece.foreach(piece => {
      piece.setHighlight(false)
      for {
        x <- 0 until 8
        y <- 0 until 8
      } {
        tiles(x)(y).setHighlight(false)
      }
    })
    selectedPiece = None
  }

}
