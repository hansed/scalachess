package org.hansed.chess.board.visual

import org.hansed.chess._
import org.hansed.chess.board.Piece
import org.hansed.engine.core.eventSystem.Clickable
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.saving.ModelDefinition

/**
  * A piece with a 3d model related to it
  *
  * @param model     the name of the model file
  * @param pieceType type of the piece
  * @param color     the color of the piece
  * @author Hannes Sederholm
  */
class ModelPiece(model: String, val pieceType: PieceType, val color: PieceColor) extends Model(
  ModelDefinition(model, "", animated = false,
    if (color == WHITE) whiteMaterial else blackMaterial
  )) with Piece with Clickable {

  if (color == WHITE) {
    rotateGlobalY(180)
  }

  /**
    * The tile this piece is on if any
    *
    * @return the coordinate of the tile
    */
  def tile: Option[(Int, Int)] = {
    parent.map(p => (p.position.x.toInt, -p.position.z.toInt))
  }

}
