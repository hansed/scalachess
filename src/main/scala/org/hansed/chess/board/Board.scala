package org.hansed.chess.board

import org.hansed.chess._
import org.hansed.chess.board.Board.MadeMove
import org.hansed.chess.moves.ChessMove
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.eventSystem.events.Event

import scala.collection.mutable

/**
  * A trait defining a chess board
  *
  * @author Hannes Sederholm
  */
trait Board[T <: Piece] {

  /**
    * Keeps track of moves currently performed
    */
  val log: mutable.Stack[ChessMove] = mutable.Stack()

  /**
    * Numeric index for the current turn
    */
  var turnIndex: Int = 0

  /**
    * The color of whose turn it currently is
    */
  var _turn: PieceColor = WHITE

  /**
    * Reset the Board to starting position
    */
  def reset(): Unit = {
    turn = WHITE
    turnIndex = 0
    for {
      x <- 0 until 8
      y <- 0 until 8
    } {
      removePiece(Point(x, y))

      (x, y) match {
        case (_, 0) | (_, 7) =>
          val color = if (y == 0) WHITE else BLACK
          x match {
            case 0 | 7 => addPiece(createPiece(ROOK, color), Point(x, y))
            case 1 | 6 => addPiece(createPiece(KNIGHT, color), Point(x, y))
            case 2 | 5 => addPiece(createPiece(BISHOP, color), Point(x, y))
            case 3 => addPiece(createPiece(QUEEN, color), Point(x, y))
            case 4 => addPiece(createPiece(KING, color), Point(x, y))
          }
        case (_, 1) => addPiece(createPiece(PAWN, WHITE), Point(x, y))
        case (_, 6) => addPiece(createPiece(PAWN, BLACK), Point(x, y))
        case (_, _) =>
      }
    }
  }

  /**
    * Perform a given move
    *
    * @param move the move
    */
  def performMove(move: ChessMove): Unit = {
    move.perform(this)
    clearCaches()
    // Block moves that end in undo
    if (inCheck()) {
      move.undo(this)
    } else {
      log.push(move)
      swapTurn()
      turnIndex += 1
      Events.signal(MadeMove(move, this))
    }
  }

  /**
    * Clear all stored computed values
    */
  def clearCaches(): Unit = {
    whiteDangerTiles.clear()
    blackDangerTiles.clear()
    var i = 0
    while (i < 64) {
      availableTilesCache(i).clear()
      availableDangerTilesCache(i).clear()
      i += 1
    }
  }

  /**
    * Check if the board is in check
    *
    * @return whether the board state is check
    */
  def inCheck(): Boolean = {
    val tiles = dangerTiles(turn)
    for {
      x <- 0 until 8
      y <- 0 until 8
    } {
      if (apply(x, y).exists(p => p.pieceType == KING && p.color == turn)) {
        return tiles.contains(Point(x, y))
      }
    }
    false
  }

  /**
    * Change the color of whose turn it currently is
    */
  def swapTurn(): Unit = {
    if (turn == WHITE) {
      turn = BLACK
    } else {
      turn = WHITE
    }
  }

  /**
    * Undo the previous move
    */
  def undo(): Unit = {
    if (log.nonEmpty) {
      log.pop().undo(this)
      clearCaches()
      swapTurn()
      turnIndex -= 1
    }
  }

  def turn: PieceColor = _turn

  def turn_=(color: PieceColor): Unit = {
    _turn = color
  }

  val availableTilesCache: Array[mutable.Set[Point]] = Array.fill(64)(mutable.HashSet())
  val availableDangerTilesCache: Array[mutable.Set[Point]] = Array.fill(64)(mutable.HashSet())

  /**
    * The tiles that the piece at given tile can move to
    *
    * @param tile   the tile
    * @param danger whether or not to only include the tiles where the opposing king would be in danger
    * @return Set of Points
    */
  def availableTiles(tile: Point, danger: Boolean = false): mutable.Set[Point] = {
    val set = if (danger) availableDangerTilesCache(tile.x + tile.y * 8) else availableTilesCache(tile.x + tile.y * 8)
    if (set.isEmpty) {
      apply(tile) match {
        case Some(piece) =>
          piece.pieceType match {
            case KING => kingTiles(tile, piece.color, danger, set)
            case QUEEN => queenTiles(tile, piece.color, set)
            case KNIGHT => knightTiles(tile, piece.color, set)
            case ROOK => rookTiles(tile, piece.color, set)
            case BISHOP => bishopTiles(tile, piece.color, set)
            case PAWN => pawnTiles(tile, piece.color, danger, set)
          }
        case _ =>
      }
    }
    set
  }

  /**
    * The available tiles of a queen of given color at given tile
    *
    * @param tile  the tile
    * @param color the color of the piece
    * @return a set of tiles that the piece can move to
    */
  def queenTiles(tile: Point, color: PieceColor, result: mutable.Set[Point]): mutable.Set[Point] = {
    bishopTiles(tile, color, result)
    rookTiles(tile, color, result)
    result
  }

  /**
    * The available tiles of a king of given color at given tile
    *
    * @param tile  the tile
    * @param color the color of the piece
    * @return a set of tiles that the piece can move to
    */
  def kingTiles(tile: Point, color: PieceColor, danger: Boolean, result: mutable.Set[Point]): mutable.Set[Point] = {

    val (x, y) = (tile.x, tile.y)

    // Basic moves
    def checkBasic(xOffset: Int, yOffset: Int) = {
      if (validTile(x + xOffset, y + yOffset) && !apply(x + xOffset, y + yOffset).exists(_.color == color)) {
        result += Point(x + xOffset, y + yOffset)
      }
    }

    checkBasic(-1, -1)
    checkBasic(-1, 0)
    checkBasic(-1, 1)
    checkBasic(0, -1)
    checkBasic(0, 1)
    checkBasic(1, -1)
    checkBasic(1, 0)
    checkBasic(1, 1)

    // Castling
    if (apply(tile).forall(_.previousTile.isEmpty) && !danger) {
      val rightPossible = apply(7, y).exists(p => p.pieceType == ROOK && p.previousTile.isEmpty)
      val leftPossible = apply(0, y).exists(p => p.pieceType == ROOK && p.previousTile.isEmpty)

      if (rightPossible || leftPossible) {
        val dTiles: mutable.Set[Point] = dangerTiles(color)

        if (rightPossible) {
          var valid = true
          var x = tile.x + 1
          while (x < 7) {
            valid = valid && (apply(x, y).isEmpty && !dTiles.contains(Point(x, y)))
            x += 1
          }
          if (valid) {
            result += Point(6, y)
          }
        }
        if (leftPossible) {
          var valid = true
          var x = tile.x - 1
          while (x > 0) {
            valid = valid && (apply(x, y).isEmpty && !dTiles.contains(Point(x, y)))
            x -= 1
          }
          if (valid) {
            result += Point(2, y)
          }
        }

      }

    }

    result
  }

  /**
    * The available tiles of a knight of given color at given tile
    *
    * @param tile  the tile
    * @param color the color of the piece
    * @return a set of tiles that the piece can move to
    */
  def knightTiles(tile: Point, color: PieceColor, result: mutable.Set[Point]): mutable.Set[Point] = {

    def check(x: Int, y: Int): Unit = {
      if (validTile(x, y) && apply(x, y).forall(_.color != color)) result += Point(x, y)
    }

    val (x, y) = (tile.x, tile.y)

    check(x + 1, y + 2)
    check(x - 1, y + 2)
    check(x + 1, y - 2)
    check(x - 1, y - 2)
    check(x + 2, y + 1)
    check(x + 2, y - 1)
    check(x - 2, y + 1)
    check(x - 2, y - 1)

    result
  }

  /**
    * The available tiles of a rook of given color at given tile
    *
    * @param tile  the tile
    * @param color the color of the piece
    * @return a set of tiles that the piece can move to
    */
  def rookTiles(tile: Point, color: PieceColor, result: mutable.Set[Point]): mutable.Set[Point] = {
    rayTiles(tile, color, 1, 0, result)
    rayTiles(tile, color, 0, 1, result)
    rayTiles(tile, color, -1, 0, result)
    rayTiles(tile, color, 0, -1, result)
    result
  }

  /**
    * The available tiles of a bishop of given color at given tile
    *
    * @param tile  the tile
    * @param color the color of the piece
    * @return a set of tiles that the piece can move to
    */
  def bishopTiles(tile: Point, color: PieceColor, result: mutable.Set[Point]): mutable.Set[Point] = {
    rayTiles(tile, color, 1, -1, result)
    rayTiles(tile, color, 1, 1, result)
    rayTiles(tile, color, -1, -1, result)
    rayTiles(tile, color, -1, 1, result)
    result
  }

  /**
    * Find the available unrestricted tiles to a given direction
    *
    * @param tile    the starting tile
    * @param color   color of piece at tile
    * @param xOffset x direction
    * @param yOffset y direction
    * @return set of tiles available
    */
  def rayTiles(tile: Point, color: PieceColor, xOffset: Int, yOffset: Int, result: mutable.Set[Point]): mutable
  .Set[Point] = {

    def check(x: Int, y: Int): Boolean = {
      if (apply(x, y).nonEmpty) {
        if (apply(x, y).forall(_.color != color)) {
          result += Point(x, y)
        }
        true
      } else {
        result += Point(x, y)
        false
      }
    }

    var x = tile.x + xOffset
    var y = tile.y + yOffset

    while (validTile(x, y) && !check(x, y)) {
      x += xOffset
      y += yOffset
    }

    result
  }

  /**
    * The available tiles of a pawn of given color at given tile
    *
    * @param tile   the tile
    * @param color  the color of the piece
    * @param danger whether to only include the tiles where opposing king would be in danger
    * @return a set of tiles that the piece can move to
    */
  def pawnTiles(tile: Point, color: PieceColor, danger: Boolean, result: mutable.Set[Point]): mutable.Set[Point] = {

    val yDif = if (color == WHITE) 1 else -1
    val (x, y) = (tile.x, tile.y)

    if (validTile(x, y + yDif)) {

      // Forward
      if (apply(x, y + yDif).isEmpty && !danger) {
        result += Point(x, y + yDif)
        // Two tiles at start
        if ((y - yDif == 7 || y - yDif == 0) && apply(x, y + 2 * yDif).isEmpty) {
          result += Point(x, y + 2 * yDif)
        }
      }

      // Diagonals
      if (x < 7 && apply(x + 1, y + yDif).exists(_.color != color)) {
        result += Point(x + 1, y + yDif)
      }
      if (x > 0 && apply(x - 1, y + yDif).exists(_.color != color)) {
        result += Point(x - 1, y + yDif)
      }

      // Passing
      def checkPassing(xOffset: Int): Unit = {
        if (validTile(x + xOffset, y + yDif) && (if (color == WHITE) y == 4 else y == 3)) {
          if (apply(x + xOffset, y + yDif).isEmpty && apply(x + xOffset, y).exists(p => {
            p.pieceType == PAWN &&
              p.color != color &&
              p.previousTile.exists(point => point.x == x + xOffset && point.y == y + 2 * yDif) &&
              p.lastMoved == turnIndex - 1
          })) {
            result += Point(x + xOffset, y + yDif)
          }
        }
      }

      if (!danger) {
        checkPassing(-1)
        checkPassing(1)
      }
    }

    result
  }

  var whiteDangerTiles: mutable.Set[Point] = mutable.HashSet()
  var blackDangerTiles: mutable.Set[Point] = mutable.HashSet()

  /**
    * Find the tiles where a piece of given color is under threat
    *
    * @param color the color of the query piece
    * @return set of tiles that are under siege
    */
  def dangerTiles(color: PieceColor): mutable.Set[Point] = {

    val result = if (color == WHITE) whiteDangerTiles else blackDangerTiles

    if (result.isEmpty) {
      val otherColor = if (color == WHITE) BLACK else WHITE

      var x = 0
      while (x < 8) {
        var y = 0
        while (y < 8) {
          if (apply(x, y).exists(_.color == otherColor)) {
            result ++= availableTiles(Point(x, y), danger = true)
          }
          y += 1
        }
        x += 1
      }
    }

    result
  }

  /**
    * Check if a tile is a valid tile of a chess board eg. is withing an 0 indexed 8x8 grid
    *
    * @param x coordinate
    * @param y coordinate
    * @return whether the tile is valid
    */
  def validTile(x: Int, y: Int): Boolean = {
    x >= 0 && y >= 0 && x < 8 && y < 8
  }

  /**
    * Create an instance of a given piece type
    *
    * @param pieceType the type of the piece
    * @param color     the color of the piece
    * @return an instance of piece for this board
    */
  def createPiece(pieceType: PieceType, color: PieceColor): T

  /**
    * Create an piece instance from another piece
    *
    * @param piece to copy
    * @return a piece
    */
  def createFromPiece(piece: Piece): T = {
    val p: T = createPiece(piece.pieceType, piece.color)
    p.lastMoved = piece.lastMoved
    p.previousTile = piece.previousTile
    p
  }

  /**
    * Move a piece from one position to other
    *
    * Removes any piece at the target position
    *
    * @param from starting point
    * @param to   ending point
    */
  def movePiece(from: Point, to: Point): Unit

  /**
    * Remove the piece at given point
    *
    * @param at the tile to clear
    */
  def removePiece(at: Point): Unit

  /**
    * Add a piece to the board
    *
    * @param piece the piece
    * @param to    the point
    */
  def addPiece(piece: Piece, to: Point): Unit

  /**
    * Get piece at given point
    *
    * @param point the point
    * @return piece at point or None if nothing is located at the point
    */
  def apply(point: Point): Option[T] = apply(point.x, point.y)

  /**
    * Get piece at given coordinates
    *
    * @param x coordinate
    * @param y coordinate
    * @return the piece at point or None if nothing is located at coordinates
    */
  def apply(x: Int, y: Int): Option[T]

}

object Board {

  /**
    * Sending this event signifies that given color should perform a move
    *
    * @param color the color whose turn it is
    */
  case class MakeMove(color: PieceColor) extends Event

  /**
    * Sent by the board once a move is performed
    *
    * @param move  the move performed
    * @param board the board that sent the event
    */
  case class MadeMove(move: ChessMove, board: Board[_ <: Piece]) extends Event

}