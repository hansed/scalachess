package org.hansed.chess.ui

import org.hansed.chess._
import org.hansed.chess.ui.PromotionMenu.{SelectPromotion, SelectedPromotion}
import org.hansed.engine.Engine
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.containers.TitledPanel
import org.hansed.engine.rendering.ui.input.Button

/**
  * The menu displayed to the user when a selection is required to promote a pawn
  *
  * @author Hannes Sederholm
  */
class PromotionMenu extends UI(0, 0, Engine.windowSize._1, Engine.windowSize._2) {

  val promotionOptions: Vector[String] = Vector("Queen", "Rook", "Bishop", "Knight")

  val promotionMenu: TitledPanel = new TitledPanel(
    Engine.windowSize._1 / 2 - 100,
    100,
    200,
    300,
    "Select promotion"
  )

  promotionMenu.visible = false

  add(promotionMenu)

  Range(0, 4).foreach(i => {
    val btn = new Button(s"${promotionOptions(i)}", 180, 50, 10, 50 + i * 60) {

      textAlign = UI.TEXT_ALIGN_CENTER

      override protected def onClick(button: PieceColor): Boolean = {
        Events.signal(SelectedPromotion(i match {
          case 0 => QUEEN
          case 1 => ROOK
          case 2 => BISHOP
          case 3 => KNIGHT
        }))
        promotionMenu.visible = false
        true
      }
    }
    promotionMenu.add(btn)
  })

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    super.processEvent(event)
    event match {
      case _: SelectPromotion =>
        promotionMenu.visible = true
      case _ =>
    }
  }
}

object PromotionMenu {

  case class SelectPromotion() extends Event

  case class SelectedPromotion(promotion: PieceType) extends Event

}
