package org.hansed.chess.moves

import org.hansed.chess.board.{Board, Piece}

/**
  * Interface for a move in the game of chess
  *
  * @author Hannes Sederholm
  */
trait ChessMove {

  /**
    * Apply the move
    */
  def perform(board: Board[_ <: Piece]): Unit

  /**
    * Undo the move
    */
  def undo(board: Board[_ <: Piece]): Unit

}
