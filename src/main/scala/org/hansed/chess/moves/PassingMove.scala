package org.hansed.chess.moves

import org.hansed.chess.Point
import org.hansed.chess.board.{Board, Piece}

/**
  * Implements the passing move of pawn
  *
  * @param from the tile the pawn starts from
  * @param to   the tile it ends up at
  * @author Hannes Sederholm
  */
case class PassingMove(from: Point, to: Point) extends ChessMove {

  /**
    * Piece removed during the move
    */
  var removedPiece: Option[Piece] = None

  /**
    * Piece moved during the move
    */
  var movedPiece: Option[Piece] = None

  /**
    * Apply the move
    */
  override def perform(board: Board[_ <: Piece]): Unit = {
    removedPiece = board(to.x, from.y)
    movedPiece = board(from.x, from.y)
    board.movePiece(from, to)
    board.removePiece(Point(to.x, from.y))
  }

  /**
    * Undo the move
    */
  override def undo(board: Board[_ <: Piece]): Unit = {
    board.removePiece(to)
    movedPiece.foreach(piece => {
      board.addPiece(piece, from)
    })
    removedPiece.foreach(piece => {
      board.addPiece(piece, Point(to.x, from.y))
    })
  }
}
