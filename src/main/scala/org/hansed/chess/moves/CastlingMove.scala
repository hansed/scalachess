package org.hansed.chess.moves

import org.hansed.chess.board.{Board, Piece}
import org.hansed.chess.{PieceColor, Point, WHITE}

/**
  * Represents the castling move
  *
  * @param color     color of the performer
  * @param queenSide whether or not the castling was done towards the queen
  * @author Hannes Sederholm
  */
case class CastlingMove(color: PieceColor, queenSide: Boolean) extends ChessMove {

  var tower: Option[Piece] = None
  var king: Option[Piece] = None

  /**
    * Apply the move
    */
  override def perform(board: Board[_ <: Piece]): Unit = {
    val y = if (color == WHITE) 0 else 7
    king = board(4, y).map(_.copy)
    if (queenSide) {
      tower = board(0, y).map(_.copy)
      board.movePiece(Point(0, y), Point(3, y))
      board.movePiece(Point(4, y), Point(2, y))
    } else {
      tower = board(7, y).map(_.copy)
      board.movePiece(Point(7, y), Point(5, y))
      board.movePiece(Point(4, y), Point(6, y))
    }
  }

  /**
    * Undo the move
    */
  override def undo(board: Board[_ <: Piece]): Unit = {
    val y = if (color == WHITE) 0 else 7
    king.foreach(p => board.addPiece(p, Point(4, y)))
    if (queenSide) {
      board.removePiece(Point(3, y))
      board.removePiece(Point(2, y))
      tower.foreach(p => board.addPiece(p, Point(0, y)))
    } else {
      board.removePiece(Point(5, y))
      board.removePiece(Point(6, y))
      tower.foreach(p => board.addPiece(p, Point(7, y)))
    }
  }
}
