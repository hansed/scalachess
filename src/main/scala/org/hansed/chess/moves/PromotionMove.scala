package org.hansed.chess.moves

import org.hansed.chess.board.{Board, Piece}
import org.hansed.chess.{PieceColor, PieceType, Point}

/**
  * Promote a pawn to higher rank
  *
  * @param from     tile of pawn
  * @param to       tile of pawn (the tile the promoted piece will appear on)
  * @param promoted the promoted type of the piece
  * @param color    the color of the piece
  * @author Hannes Sederholm
  */
case class PromotionMove(from: Point, to: Point, promoted: PieceType, color: PieceColor) extends ChessMove {

  /**
    * The piece removed (eg. the pawn)
    */
  var removedPiece: Option[Piece] = None
  var movedPiece: Option[Piece] = None

  /**
    * Apply the move
    */
  override def perform(board: Board[_ <: Piece]): Unit = {
    removedPiece = board(to).map(_.copy)
    movedPiece = board(from).map(_.copy)
    board.removePiece(from)
    board.removePiece(to)
    board.addPiece(board.createPiece(promoted, color), to)
  }

  /**
    * Undo the move
    */
  override def undo(board: Board[_ <: Piece]): Unit = {
    board.removePiece(to)
    removedPiece.foreach(piece => {
      board.addPiece(piece, to)
    })
    movedPiece.foreach(piece => {
      board.addPiece(piece, from)
    })
  }
}
