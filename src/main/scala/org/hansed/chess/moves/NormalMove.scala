package org.hansed.chess.moves

import org.hansed.chess.Point
import org.hansed.chess.board.{Board, Piece}

/**
  * Normal chess move
  *
  * Moves the piece from given point to another and removes the piece under the new point
  *
  * @param from starting tile
  * @param to   target tile
  * @author Hannes Sederholm
  */
case class NormalMove(from: Point, to: Point) extends ChessMove {

  /**
    * The piece removed during the move
    */
  var removedPiece: Option[Piece] = None
  var movedPiece: Option[Piece] = None

  /**
    * Apply the move
    */
  override def perform(board: Board[_ <: Piece]): Unit = {
    removedPiece = board(to).map(_.copy)
    movedPiece = board(from).map(_.copy)
    board.movePiece(from, to)
  }

  /**
    * Undo the move
    */
  override def undo(board: Board[_ <: Piece]): Unit = {
    board.removePiece(to)
    movedPiece.foreach(p => {
      board.addPiece(p, from)
    })
    removedPiece.foreach(p => {
      board.addPiece(p, to)
    })
  }
}
