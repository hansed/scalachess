package org.hansed

import org.hansed.engine.saving.MaterialDefinition

/**
  * Constants for the chess game
  */
package object chess {

  type PieceColor = Int

  val WHITE: PieceColor = 0
  val BLACK: PieceColor = 1
  val BOTH: PieceColor = 2

  val blackMaterial = Some(MaterialDefinition("textures/black.png", None))
  val whiteMaterial = Some(MaterialDefinition("textures/white.png", None))

  type PieceType = Int

  val KING: PieceType = 0
  val QUEEN: PieceType = 1
  val KNIGHT: PieceType = 2
  val ROOK: PieceType = 3
  val BISHOP: PieceType = 4
  val PAWN: PieceType = 5

}
