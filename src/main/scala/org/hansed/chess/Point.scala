package org.hansed.chess

/**
  * Shorthand for a 2 integer coordinate
  *
  * @param x the x coordinate
  * @param y the y coordiante
  * @author Hannes Sederholm
  */
case class Point(x: Int, y: Int)
