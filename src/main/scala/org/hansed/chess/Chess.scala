package org.hansed.chess

import org.hansed.chess.board.visual.VisualBoard
import org.hansed.engine.Flags.{SHUTDOWN, VERBOSE_DEBUG}
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.{KeyDown, KeyUp}
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.core.input.MousePicker
import org.hansed.engine.core.{GameObject, GameScene}
import org.hansed.engine.rendering.RenderingEvents.FocusEntityCamera
import org.hansed.engine.rendering.camera.{Camera, EntityFollowingCamera}
import org.hansed.engine.rendering.lights.PointLight
import org.hansed.engine.rendering.shaders.HitBoxShader
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.containers.TitledPanel
import org.hansed.engine.rendering.ui.input.Button
import org.hansed.engine.{Engine, Flags}
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW._

/**
  * Chess game
  *
  * @author Hannes Sederholm
  */
object Chess extends App {

  val (windowWidth, windowHeight) = (1280, 720)

  Engine.init(windowWidth, windowHeight, "Chess")

  Engine.scene = GameScene(
    new EntityFollowingCamera(Camera.perspective(windowWidth, windowHeight, fov = 25.0f)) {
      horizontalSpin = -0.40f
      radius = 15.0f
    })


  val root = Engine.scene.rootObject

  root.addComponent(MousePicker)
  MousePicker.enabled = true
  root.addChild(PointLight(new Vector3f(1, 1, 1), power = 4.0f, offset = new Vector3f(3.5f, 6, -3.5f)))

  val cameraTarget = new GameObject() {
    position = new Vector3f(3.5f, 1, -3.5f)
  }

  root.addChild(cameraTarget)

  Events.signal(FocusEntityCamera(cameraTarget, autoUpdate = true))

  val ui = new UI(0, 0, windowWidth, windowHeight)

  root.addChild(ui)

  ui.add(new TitledPanel(200, 100, windowWidth - 400, 300, "Select mode") {
    add(new Button("Player vs Player", windowWidth - 450, 50, 25, 80) {
      override protected def onClick(button: PieceColor): Boolean = {
        root.addChild(new VisualBoard(None))
        ui.remove()
        true
      }
    })
    add(new Button("Player vs Computer", windowWidth - 450, 50, 25, 150) {
      override protected def onClick(button: PieceColor): Boolean = {
        val computer = new ComputerPlayer()
        root.addChild(computer)
        root.addChild(new VisualBoard(Some(computer)))
        ui.remove()
        true
      }
    })
    add(new Button("Computer vs Computer", windowWidth - 450, 50, 25, 220) {
      override protected def onClick(button: PieceColor): Boolean = {
        val computer = new ComputerPlayer(color = BOTH)
        root.addChild(computer)
        root.addChild(new VisualBoard(Some(computer)))
        ui.remove()
        true
      }
    })
  })

  val defaultShaders = Engine.shaders.clone()

  root.addChild(new GameObject with EventListener {
    override def processEvent(event: Event): Unit = {
      event match {
        case KeyUp(GLFW_KEY_ESCAPE) =>
          Flags(SHUTDOWN) = true
        case KeyDown(GLFW_KEY_I) =>
          if (!Engine.shaders.contains(HitBoxShader)) {
            Engine.shaders.clear()
            Engine.shaders += HitBoxShader
          }
        case KeyDown(GLFW_KEY_O) =>
          if (Engine.shaders.contains(HitBoxShader)) {
            Engine.shaders.clear()
            Engine.shaders ++= defaultShaders
          }
        case KeyDown(GLFW_KEY_C) =>
          val cam = Engine.camera.asInstanceOf[EntityFollowingCamera]
          cam.controllable = !cam.controllable
        case KeyDown(GLFW_KEY_D) =>
          Flags(VERBOSE_DEBUG) = !Flags(VERBOSE_DEBUG)
        case _ =>
      }
    }
  })

  Engine.run()

}
