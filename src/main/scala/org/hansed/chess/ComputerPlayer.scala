package org.hansed.chess

import org.hansed.chess.board.Board.{MadeMove, MakeMove}
import org.hansed.chess.board.computer.MoveTree
import org.hansed.chess.board.{Board, Piece}
import org.hansed.chess.evaluation.{BasicEvaluator, Evaluator}
import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.util.Debug

/**
  * Represents the computer
  *
  * @param evaluator the evaluator used for evaluating the moves
  * @author Hannes Sederholm
  */
class ComputerPlayer(val color: PieceColor = BLACK, depth: Int = 4)(implicit val evaluator: Evaluator = BasicEvaluator)
  extends GameObject with EventListener {

  var board: Board[_ <: Piece] = _

  var moveTree: MoveTree = MoveTree(WHITE)

  /**
    * Select the best move for the current turn and play it on the board
    *
    */
  def makeMove(): Unit = {
    Debug.logAction(() => moveTree.update(board, depth), s"Evaluating board for turn ${board.turnIndex}", this)
    moveTree.bestMove.foreach(board.performMove)
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case MakeMove(color) =>
        if (this.color == color || this.color == BOTH) {
          makeMove()
        }
      case MadeMove(move, b) =>
        if (b == board) {
          moveTree = moveTree.select(b, move)
        }
      case _ =>
    }
  }
}
