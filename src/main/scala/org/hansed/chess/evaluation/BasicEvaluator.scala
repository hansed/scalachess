package org.hansed.chess.evaluation

import org.hansed.chess._
import org.hansed.chess.board.{Board, Piece}

/**
  * Basic evaluator for evaluating the state of the game
  *
  * @author Hannes Sederholm
  */
object BasicEvaluator extends Evaluator {

  val materialWeights: Map[PieceType, Float] = Map(
    KING -> 200.0f,
    QUEEN -> 9.0f,
    ROOK -> 5.0f,
    BISHOP -> 3.0f,
    KNIGHT -> 3.0f,
    PAWN -> 1.0f,
  )

  val multiplier: Map[PieceColor, Float] = Map(
    WHITE -> 1,
    BLACK -> -1
  )

  val mobilityWeight: Float = 0.1f

  val doubledWeight: Float = 0.5f

  val isolatedWeight: Float = 0.5f

  val blockedWeight: Float = 0.5f

  val kingThreatWeight: Float = 200.0f


  override def evaluate(board: Board[_ <: Piece]): Float = {

    var materialScore = 0.0f

    val whitePawnCounts = Array.fill[Int](8)(0)
    val blackPawnCounts = Array.fill[Int](8)(0)

    var whiteKingThreatened: Boolean = false
    var blackKingThreatened: Boolean = false

    var doubledPawns: Int = 0
    var blockedPawns: Int = 0

    var availableMoves = 0

    var x = 0
    while (x < 8) {
      var y = 0
      while (y < 8) {
        board(x, y).foreach(p => {
          if (p.color == WHITE) {
            materialScore += materialWeights(p.pieceType)
            p.pieceType match {
              case PAWN =>
                whitePawnCounts(x) += 1
                if (whitePawnCounts(x) > 1) doubledPawns += 1
                if (board.validTile(x, y + 1) && board(x, y + 1).nonEmpty) blockedPawns += 1
              case KING =>
                whiteKingThreatened = board.dangerTiles(WHITE).contains(Point(x, y))
              case _ =>
            }
            availableMoves += board.availableTiles(Point(x, y)).size
          } else {
            materialScore -= materialWeights(p.pieceType)
            p.pieceType match {
              case PAWN =>
                blackPawnCounts(x) += 1
                if (blackPawnCounts(x) > 1) doubledPawns -= 1
                if (board.validTile(x, y - 1) && board(x, y - 1).nonEmpty) blockedPawns -= 1
              case KING =>
                blackKingThreatened = board.dangerTiles(BLACK).contains(Point(x, y))
              case _ =>
            }
            availableMoves -= board.availableTiles(Point(x, y)).size
          }
        })
        y += 1
      }
      x += 1
    }

    var isolatedPawns = 0

    x = 0
    while (x < 8) {
      if (whitePawnCounts(x) > 0) {
        if (x == 0) {
          if (whitePawnCounts(x + 1) == 0) isolatedPawns += 1
        } else if (x == 7) {
          if (whitePawnCounts(x - 1) == 0) isolatedPawns += 1
        } else {
          if (whitePawnCounts(x - 1) == 0 && whitePawnCounts(x + 1) == 0) isolatedPawns += 1
        }
      }
      if (blackPawnCounts(x) > 0) {
        if (x == 0) {
          if (blackPawnCounts(x + 1) == 0) isolatedPawns -= 1
        } else if (x == 7) {
          if (blackPawnCounts(x - 1) == 0) isolatedPawns -= 1
        } else {
          if (blackPawnCounts(x - 1) == 0 && blackPawnCounts(x + 1) == 0) isolatedPawns -= 1
        }
      }
      x += 1
    }

    materialScore +
      doubledPawns * doubledWeight +
      isolatedPawns * isolatedWeight +
      blockedPawns * blockedWeight +
      availableMoves * mobilityWeight +
      (if (whiteKingThreatened) kingThreatWeight else 0) +
      (if (blackKingThreatened) -kingThreatWeight else 0)
  }
}
