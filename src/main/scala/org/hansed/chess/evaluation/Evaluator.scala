package org.hansed.chess.evaluation

import org.hansed.chess.board.{Board, Piece}

/**
  * Interface for an evaluator for the state of the board
  *
  * @author Hannes Sederholm
  */
trait Evaluator {

  def evaluate(board: Board[_ <: Piece]): Float

}
